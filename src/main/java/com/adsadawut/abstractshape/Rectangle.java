/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.abstractshape;

/**
 *
 * @author อัษฎาวุฒิ
 */
public class Rectangle extends Shape{
    protected double width;
    protected double length;
    
    public Rectangle(double width,double length){
        super("Rectangle");
        this.width=width;
        this.length=length;
    }

    public double getWidth() {
        return width;
    }

    public double getLength() {
        return length;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public void setLength(double length) {
        this.length = length;
    }
    
    @Override
    public double calArea(){
        return width*length;
    }

    @Override
    public String toString() {
        return "Rectangle{" + "w=" + width + ", l=" + length + '}';
    }
    
    
}
